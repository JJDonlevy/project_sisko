﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    InkManager inkManager;
    // Start is called before the first frame update
    private void Awake()
    {
        inkManager = GetComponent<InkManager>();
    }
    void Start()
    {
        if (SceneManager.GetActiveScene().name == "SampleScene")
        {
            inkManager.enabled = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
