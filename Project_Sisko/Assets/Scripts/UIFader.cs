﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIFader : MonoBehaviour
{
    public CanvasGroup uiElement;
    public Text prompt;
    public void FadeIn()
    {
        StartCoroutine(FadeCanvasGroup(uiElement, uiElement.alpha, 1));
    }

    public void FadeOut()
    {
        StartCoroutine(FadeCanvasGroup(uiElement, uiElement.alpha, 0));
    }

    public void Wait(int a_timeToWait, string a_sentence)
    {
        StartCoroutine(Waiting(a_timeToWait, a_sentence));
        

    }

    public IEnumerator Waiting(float a_wait, string a_sentence)
    {
        StartCoroutine(FadeCanvasGroup(uiElement, uiElement.alpha, 1));
        prompt.text = a_sentence;
        yield return new WaitForSeconds(a_wait);
        StartCoroutine(FadeCanvasGroup(uiElement, uiElement.alpha, 0));
    }

    public IEnumerator FadeCanvasGroup(CanvasGroup a_cg, float a_start, float a_end, float a_lerpTime = 0.25f)
    {
        float timeStartedLerping = Time.time;
        float timeSinceStarted = Time.time - timeStartedLerping;

        float percentageComplete = timeSinceStarted / a_lerpTime;

        while (true)
        {
            timeSinceStarted = Time.time - timeStartedLerping;
            percentageComplete = timeSinceStarted / a_lerpTime;

            float currentValue = Mathf.Lerp(a_start, a_end, percentageComplete);

            a_cg.alpha = currentValue;

            if (percentageComplete >= 1)
                break;

            yield return new WaitForEndOfFrame();
        }
        
    }
}
