﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ResourcesCounter : MonoBehaviour
{
	//Set manually
	public ResourceGenerator resourceGenerator;

	TextMeshProUGUI textMesh;

	private void Start()
	{
		textMesh = GetComponent<TextMeshProUGUI>();
	}

	// Update is called once per frame
	void Update()
    {
		textMesh.text = "Resources: " + resourceGenerator.resources;
    }
}
