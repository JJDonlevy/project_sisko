﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[RequireComponent(typeof(TextMeshProUGUI))]
public class TurnCounter : MonoBehaviour
{
	//Set this manually in inspector
	public EndTurn endTurnHandler;

	TextMeshProUGUI textMesh;

	private void Start()
	{
		textMesh = GetComponent<TextMeshProUGUI>();
	}

	// Update is called once per frame
	void Update()
    {
		textMesh.text = "Turn: " + endTurnHandler.turnNumber;
    }
}
