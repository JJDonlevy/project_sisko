﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[RequireComponent(typeof(TextMeshProUGUI))]
public class SubfactionPopulationText : MonoBehaviour
{
	//Set this variable manually in inspector
	public Population population;
	//Set this variable manually in inspector to be one of {0,1,2}
	public int subfactionID = 0;

	TextMeshProUGUI textMesh;

	private void Start()
	{
		textMesh = GetComponent<TextMeshProUGUI>();
	}

	// Update is called once per frame
	void Update()
    {
		textMesh.text = population.subfactions[subfactionID].name + ": " + population.subfactions[subfactionID].allocatedPop;
    }
}
