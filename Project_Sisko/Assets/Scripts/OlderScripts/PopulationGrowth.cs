﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopulationGrowth : MonoBehaviour, InterfaceEndTurn
{
	public int turnsTillGrowth = 3;
	void InterfaceEndTurn.EndTurnBehaviour()
	{
		//Reduce turn till growth counter by 1
		turnsTillGrowth--;

		if(turnsTillGrowth <= 0)
		{
			//Grow station crew pop
			GetComponent<Population>().subfactions[0].allocatedPop++;

			//Reset turns till growth to some limit
			turnsTillGrowth = 3;
		}
	}
}
