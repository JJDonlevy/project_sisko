﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Population))]
public class ResourceGenerator : MonoBehaviour, InterfaceEndTurn
{
	public int resources = 0;

	Population population;

	void Start()
	{
		population = GetComponent<Population>();	
	}

	public void EndTurnBehaviour()
	{
		//This function is run and will output how many resources we get next turn
		resources += ResourceNextTurnCalculation();
	}

	int ResourceNextTurnCalculation ()
	{
		int amountToGenerateNextTurn;

		//Put whatever calculations here to deal with how many resources we'll get next turn

		//For testing purposes we'll use the function y=x
		//But we could add dampening effects for things like happiness etc.

		amountToGenerateNextTurn = population.subfactions[1].allocatedPop;

		return amountToGenerateNextTurn;
	}
}
