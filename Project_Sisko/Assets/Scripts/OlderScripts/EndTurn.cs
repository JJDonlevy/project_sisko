﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndTurn : MonoBehaviour
{
	public GameObject[] managers;

	public int turnNumber = 0;

	InterfaceEndTurn[] interfaces;
	public void EndTurnMethod ()
	{
		turnNumber++;
		
		//Go through every manager in the game
		foreach(GameObject manager in managers)
		{
			//Get the interfaces attached to this gameobject which have end turn behaviours
			interfaces = manager.GetComponents<InterfaceEndTurn>();

			foreach (InterfaceEndTurn interfaceEndTurn in interfaces)
			{
				//Call the end turn behaviour defined by the interface
				interfaceEndTurn.EndTurnBehaviour();
			}
		}
	}
}
