﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Population))]
public class AllocatePopulation : MonoBehaviour, InterfaceEndTurn
{
	Population population;

	//Needs to be set manually
	public SubfactionSelection subfactionSelection;

	public int subfactionChoiceOne = 0; //Between {0,1,2}
	public int subfactionChoiceTwo = 0; //Between {0,1,2}

	public int amountChoice = 0;
	void InterfaceEndTurn.EndTurnBehaviour()
	{
		//Changes to allocated population happen here
		if(population.subfactions[subfactionChoiceOne].allocatedPop < amountChoice)
		{
			//Then wanted to take too much from one so we set amountChoice to how much there is in there
			amountChoice = population.subfactions[subfactionChoiceOne].allocatedPop;
		}

		population.subfactions[subfactionChoiceOne].allocatedPop -= amountChoice;
		population.subfactions[subfactionChoiceTwo].allocatedPop += amountChoice;

		amountChoice = 0; //Clear this variable now so when we end the turn it doesn't keep trying to allocate
	}

	// Start is called before the first frame update
	void Start()
    {
		population = GetComponent<Population>();
	}

	void Update ()
	{
		subfactionChoiceOne = subfactionSelection.choiceOne;
		subfactionChoiceTwo = subfactionSelection.choiceTwo;
		amountChoice = subfactionSelection.transferAmount;
	}
}