﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SubfactionSelection : MonoBehaviour
{
	public GameObject[] FirstChoices;
	public GameObject[] SecondChoices;

	GameObject firstChoice;
	GameObject secondChoice;

	public int choiceOne;
	public int choiceTwo;

	//Set this manually
	public TMP_InputField inputField;
	public int transferAmount;

	LineRenderer lineRenderer;
    // Start is called before the first frame update
    void Start()
    {
		lineRenderer = GetComponent<LineRenderer>();

		lineRenderer.positionCount = 2;

		lineRenderer.startWidth = 0.075f;
		lineRenderer.endWidth = 0.075f;
    }

    // Update is called once per frame
    void Update()
    {
		if(firstChoice != null && secondChoice != null)
		{
			lineRenderer.SetPosition(0, firstChoice.transform.position);
			lineRenderer.SetPosition(1, secondChoice.transform.position);
		}
    }

	public void ChoiceOne (int choice)
	{
		firstChoice = FirstChoices[choice];
		choiceOne = choice;
	}

	public void ChoiceTwo (int choice)
	{
		secondChoice = SecondChoices[choice];
		choiceTwo = choice;
	}

	public void TransferAmount ()
	{
		transferAmount = int.Parse(inputField.text);
	}
}
