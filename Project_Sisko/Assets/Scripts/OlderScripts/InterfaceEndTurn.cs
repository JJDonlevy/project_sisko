﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//This interface is implemented wherever we want behaviour when an end turn is called
public interface InterfaceEndTurn
{
	void EndTurnBehaviour();
}
