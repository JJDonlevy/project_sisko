﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActorBase : MonoBehaviour
{
    string firstName, lastName, displayName;

    [HideInInspector]
    public Sprite characterPortrait;

    //Core skills for a character
    [Range(0, 6)]
    protected uint biology = 1, engineering = 1, diplomacy = 1, trading = 1,
                security = 1, programming = 1, anthroministry = 1, xenology = 1;

    public string Test;

    public enum SkillDescribers
    {
        Useless = 0,
        Mediocre = 1,
        Fair = 2,
        Good = 3,
        Great = 4,
        Superb = 5,
        Renowned = 6
    };

    SkillDescribers Describer;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Test = DescribeSkill(Describer, biology);
    }


    /// <summary>
    /// Feed this function an SkillDescribers enum and the required stat to display the correct value without numbers
    /// </summary>
    /// <param name="a_describer"></param>
    /// <param name="a_value"></param>
    /// <returns></returns>
    public string DescribeSkill(SkillDescribers a_describer, uint a_value)
    {
        a_describer = (SkillDescribers)a_value;

        switch (a_describer)
        {
            case SkillDescribers.Useless:
                return a_describer.ToString();
            case SkillDescribers.Mediocre:
                return a_describer.ToString();
            case SkillDescribers.Fair:
                return a_describer.ToString();
            case SkillDescribers.Good:
                return a_describer.ToString();
            case SkillDescribers.Great:
                return a_describer.ToString();
            case SkillDescribers.Superb:
                return a_describer.ToString();
            case SkillDescribers.Renowned:
                return a_describer.ToString();
            default:
                Debug.LogError("ERROR with Describe Skill");
                return "-ERROR-";
        }

    }
}
