﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StationManager : MonoBehaviour
{
    public int resources;

    public UIFader uiFader;

    public Miner miner = new Miner();
    public Security security = new Security();
    public StationCrew stationCrew = new StationCrew();
    
    int displayMiner, displaySecurity, displayStationCrew;
    int minerMoral, securityMoral, stationCrewMoral;

    [Space]
    //[HideInInspector]
    public int councilBiology = 0, councilEngineering = 0, councilDiplomacy = 0, 
        councilTrading = 0, councilSecurity = 0, councilProgramming = 0,
        councilAnthroministry = 0, councilXenology = 0;

    // Start is called before the first frame update
    void Start() 
    {
        resources = 100;
    }

    // Update is called once per frame
    void Update()
    {
        displayMiner = miner.total;
        minerMoral = miner.happiness;

        displaySecurity = security.total;
        securityMoral = security.happiness;

        displayStationCrew = stationCrew.total;
        stationCrewMoral = stationCrew.happiness;
    }

    public int RemoveResources(int a_lostAmount)
    {
        uiFader.Wait(1, ("You lost " + a_lostAmount + " Resources"));
        return (resources -= a_lostAmount);
    }
    public int GainResources(int a_gainedAmount)
    {
        uiFader.Wait(1, ("You gained " + a_gainedAmount + " Resources"));
        return (resources += a_gainedAmount);
    }

    public class Faction : IFaction
    {
        int units;
        int happy;

        public int total
        {
            get { return units; }
            set { units = value; }
        }
        public int happiness
        {
            get { return happy; }
            set { happy = value; }
        }
    }
    // General Miner Components 
    public class Miner : Faction
    {
    }
    public int GainMoralMiner(int a_gainedMoral)
    {
        uiFader.Wait(1, ("The Miners were pleased!"));
        return (miner.happiness += a_gainedMoral);
    }
    public int LostMoralMiner(int a_lostMoral)
    {
        uiFader.Wait(1, ("The Miners disliked that..."));
        return (miner.happiness -= a_lostMoral);
    }
    // General Security Components 
    public class Security : Faction
    {
    }
    public int GainMoralSecurity(int a_gainedMoral)
    {
        uiFader.Wait(1, ("Security was pleased by your actions."));
        return (security.happiness += a_gainedMoral);
    }
    public int LostMoralSecurity(int a_lostMoral)
    {
        uiFader.Wait(1, ("Security does not like the choice you made."));
        return (security.happiness -= a_lostMoral);
    }
    // General Station Crew Components 
    public class StationCrew : Faction
    {
    }
    public int GainMoralStationCrew(int a_gainedMoral)
    {
        uiFader.Wait(1, ("You actions please the Station Crew."));
        return (stationCrew.happiness += a_gainedMoral);
    }
    public int LostMoralStationCrew(int a_lostMoral)
    {
        uiFader.Wait(1, ("The Station Crew did not enjoy that action."));
        return (stationCrew.happiness -= a_lostMoral);
    }
    public class Factionless : Faction
    {
    }
    public int GainMoralFactionless(int a_gainedMoral)
    {
        uiFader.Wait(1, ("They were pleased by your actions."));
        return (security.happiness += a_gainedMoral);
    }
    public int LostMoralFactionless(int a_lostMoral)
    {
        uiFader.Wait(1, ("They do not like the choice you have made."));
        return (security.happiness -= a_lostMoral);
    }
}

public interface IFaction
{
    int total
    {
        get;
        set;
    }

    int happiness
    {
        get;
        set;
    }
}