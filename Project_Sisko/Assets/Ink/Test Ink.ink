//Need an "if the character has > 0 miners" case
Samhael, one of your mining crewmembers, has been producing bootleg alcohol and selling it on the station. Furthermore, he's been doing it on shift time. Several station crew have gotten sick from drinking the bootleg booze, and they've brought the issue to the Station Council. 

//Big section for councillors opinions here. Pseudocode like: 
//COUNCILLOR_PERSONALTIY_TYPE_1: Some insightful comment that highlights a corresponding option.  
//COUNCILLOR_PERSONALITY_TYPE_1: A similar opinion highlights the same option. 
//COUNCILLOR_PERSONALITY_TYPE_1: A slightly contrary opinion, highlights a different option. 
//COUNCILLOR_PERSONALITY_TYPE_1: Fall through repeat answer for the rest of councillors of this opinion, if for some reason they have stacked their council with 1 type of councillor. Something like "You've already heard good advice from the rest of the council." 
//COUNCILLOR_PERSONALITY_TYPE_2: A useless idea. 
//COUNCILLOR_PERSONALITY_TYPE_2: A reiteration of that same useless idea. 
//Etc. for each councillor ptype 
//COUNCILLOR_CUSTOM_PERSONALITY_NAME(i.e COUNCILLOR_ROLAND): If any of the councillors have a custom personality/are a named character instead of a randomly generated one, they get their own custom dialogue. 

+ [Confiscate the scrip Samhael earned from the bootleg booze and put it in the Station resource pool.] ->ConfiscateBank
+ [Confiscate Samhael's scrip and divide it amongst the sick station crewmembers.]->ConfiscateDivide
+ [Confiscate Samhael's booze and put him on double duty.] ->ConfiscateBooze
+ ["This is for Samhael's superior to resolve."]->Superior
+ [Throw Samhael in the brig and confiscate everything.].->Brig

==ConfiscateBank==
The station crew and the miners grumbled about the unfairness of this together, and quickly forgot the animosity from the bad booze. Samhael himself seems thankful to have avoided more severe punishment. 
//UPDATE VARIABLES HERE - STATION CREW AND MINERS -HAPPY, -RESPECT, +SMALL RESOURCES
->END
==ConfiscateDivide== 
The station crew accept the extra scrip, but wanted Samhael to face more severe punishment. 
//UPDATE VARIABLES HERE - STATION CREW MINOR +HAPPY -RESPECT
->END
==ConfiscateBooze==
The double duty seems to have pleased the station crew, but the miners are annoyed that their source of illicit booze has disappeared. They seem to think that the Council should look the other way if they work hard. 
//UPDATE VARIABLES HERE - STATION CREW +MAJOR HAPPY, MINERS -HAPPY
->END
==Superior==
The station crew are furious that Samhael got away with only a light punishment from his superior, but the miners are happy that you respect their autonomy. 
//UPDATE VARIABLES HERE - STATION CREW -HAPPY -RESPECT, MINERS +RESPECT
->END
==Brig== 
The arrest seems to have quietened down the miners significantly, and they tread much lighter around the Council rules now. The station crew are smug, but seem to think that this punishment might be a bit too harsh. 
//UPDATE VARIABLES HERE -MINERS -HAPPY +RESPECT, STATION CREW +RESPECT
->END