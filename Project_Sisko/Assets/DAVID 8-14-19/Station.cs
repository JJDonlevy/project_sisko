﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "New Station")]
public class Station : ScriptableObject
{
    public int resources;

    public int turnNumber;

    public Department[] departments;

    [Header("Total Council Skill (Do not edit these here)")]
    public int councilMining;
    public int councilSecurity;


}