﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimLoop : MonoBehaviour
{
    //Stuff to remember
    //Main crew is station.departments[0]

    public Station station;

    public float currentGrowth;

    void Awake ()
    {
        //Here we calculate the department values
        foreach (Department department in station.departments)
        {
            department.CalculateBerthing();
        }
    }

    public void EndTurn ()
    {
        ResourceGeneration();
        CrewGeneration();

        //Here we calculate the department values
        foreach(Department department in station.departments)
        {
            department.CalculateBerthing();
        }

        station.turnNumber++; //This stations turn number increases by one
    }

    //This gets run at end of every turn
    //This also should only depend on the mining department + council mining skill with a decrease based on mining happiness
    void ResourceGeneration()
    {
        //Add decrease by some factor with happiness later
        Department miningDepartment = station.departments[1];
        station.resources += 3 * miningDepartment.crewAssigned + station.councilMining;
    }

    void CrewGeneration()
    {
        //We calculate how much pop we have
        int totalStationPop = 0;
        foreach(Department department in station.departments)
        {
            totalStationPop += department.crewAssigned;
        }

        currentGrowth += totalStationPop * 0.015f; //We make this a separate step incase we want to modify how this is calculated

        //Then this float contains some whole integer values in it
        if(currentGrowth >= 1)
        {
            Department crewDepartment = station.departments[0]; //General crew is always the zeroth department
            crewDepartment.crewAssigned += Mathf.FloorToInt(currentGrowth);
            currentGrowth -= Mathf.FloorToInt(currentGrowth);
        }


    }

    //Need to add specific function for adding council members to departments

    //Need to add specific function for generation of resources
}
