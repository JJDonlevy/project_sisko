﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "New Department")]
public class Department : ScriptableObject
{
    public int level;
    public int crewAssigned;
    public int berthing;

    public enum Happiness {Discontent, Content, Happy};

    public int happinessValue;

    //The int return on this function is the cost to upgrade the berthing
    //Input station resources, if the amount of station resources currently is greater than the cost, then return the cost
    public int UpgradeDepartmentLevel (int stationResources)
    {
        int upgradeCost = 21*level; //Just some simple calculation for how much the upgrade cost is

        if(stationResources >= upgradeCost)
        {
            level++;
            return upgradeCost;
        }

        return 0; 
        //We return zero because the script that calls this function will want a value to reduce the resources by and if we don't have enough resources to upgrade the berthing then we send back 0
    }

    public void CalculateBerthing ()
    {
        berthing = 5 + 37 * level;
    }
}
