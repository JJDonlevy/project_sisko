﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "New Council Member")]
public class CouncilMember : ScriptableObject
{
    public Texture2D image;
    public enum Department { Mining, Science, Security }; //This can be added to if in need of more departments

    public Department department; //This determines what department this person can be a council member of

    [Space]
    public int mining = 0, security = 0;  //Just add more here when in need of more skills
}