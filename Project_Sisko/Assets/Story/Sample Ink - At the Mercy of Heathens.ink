EXTERNAL remove_resources(lost_amount)
EXTERNAL gain_resources(lost_amount)
EXTERNAL lost_moral_miner(lost_amount)
EXTERNAL gain_moral_miner(lost_amount)
EXTERNAL lost_moral_stationCrew(lost_amount)
EXTERNAL gain_moral_stationCrew(lost_amount)
EXTERNAL lost_moral_security(lost_amount)
EXTERNAL gain_moral_security(lost_amount)
EXTERNAL lost_moral_factionless(lost_amount)
EXTERNAL gain_moral_factionless(lost_amount)

//========Global Variables============
VAR councilSkillBiology = 0
VAR councilSkillEngineering = 0
VAR councilSkillDiplomacy = 0
VAR councilSkillTrading = 0
VAR councilSkillSecurity = 0
VAR councilSkillProgramming = 0
VAR councilSkillAnthroministry = 0
VAR councilSkillXenology = 0
//====================================
//INK data with it's Plot level and Range
A mining crew salvager brought in what he thought to be a big score - A derelict ship of ancient design. At first it was believed the thing was alien it was so primitive, but analysis of its outer hull found traces of paint that once spelled out a word in a human dialect - "JEREMIAH." 

Cracking open the ship and investigating the inside found a single passenger in cryogenic freezing - Still alive, although there is no hope of resuscitating him with your meager medical facilities. On top of the freezing chamber was a small steel amulet of some kind, shaped like two intersecting bars, the horizontal one smaller than the other. 

The symbology is familiar to your anthrocultivist, Loxlen. He claims that the amulet is a religious item, and furthermore that the man inside the cryochamber is a member of this ancient religion. Loxlen belives that he can learn much of pre-space human history from speaking to this man and thereby improve his skills. 

Barrett, Chief Engineer: 'Best we leave off this one. It's salvage, rightfully claimed.'
Mateus, Head of Security: 'If the man inside is alive, it's not salvage. Let's take it off his hands.' 
//Gianna, Mining Corp Exec: 'Simply buy it from the man.' 
Barret, Chief Engineer: 'The subtleties of interstellar law may be lost on a nitro-huffing salvager.' 
Loxlen, Anthrocultivist: 'Just buy it off him, then! Quickly!'
Mateus, Head of Security: 'If we start paying for every piece of illegal junk the rock jockeys bring in, we'll be broke.' 
Karl, Trader: 'This is a waste of money, and time. Old junk rarely turns a profit.' 

* [Buy the rights to the salvage.] -> buy_rights
* ["This is not the business of the Station Council.] -> no_business
* [Confiscate the ship from the salvager.]->confiscate
* [Pressure Loxlen for details on the man's religion.]->pressure_loxlen_test


==no_business== 
//Lower Loxlen's faction's happiness
//Increase miner faction's happiness
//Give Loxlen special text about his disappointment
You leave the salvager with his find, likely to be sold on to any trader that flies through in the next year. The miners and crew respect you not meddling in their affairs, but Loxlen is quiet for the rest of the council meeting.
->END
==pressure_loxlen_test==
You put the lean on Loxlen. 
//Execute Council Test code
//Based on success or fail, push to either pressure_loxlen_success or pressure_loxlen_fail
//Below divert is here for debugging as .ink does not allow dead ends

//Jesse: So in ink this is the equivalent to an if statement and else if statement, by using this it can change the choice based on a value;
{ 
- councilSkillDiplomacy >= 20:
->pressure_loxlen_success
- councilSkillDiplomacy < 20:
->pressure_loxlen_fail
}
==pressure_loxlen_success==
Loxlen is recalcitrant, but with a bit of diplomacy, he eventually admits that he knows more about the man's religion than he initially let on. 

Loxlen: 'Uh, well.' 
Loxlen: 'I'm sure it's not really a big deal, but it turns out...' 
Loxlen: 'His religion contains a belief in an "Immortal Soul"? 
Loxlen: 'It's a very interesting concept, and perhaps the origin of the belief in modern Xenodeic religions. It's the idea that when your body dies, you lose some ephemeral part of yourself that transcends to another plane of existence - Usually punitive.' 
Loxlen: 'We don't have to tell him he's been reprinted, of course - He wouldn't understand the concept.'
Mateus: 'Well, that solves the salvage problem - Sleeper's a cultist. Rock jockeys wont want to touch that.'
*[Allow Loxlen to euthanize and reprint the ancient sleeper.] ->reprint
*[Keep the man frozen until you have the ability to resuscitate him]. ->long_freeze
*[Leave the ship and it's passenger in storage to sell off yourself]. ->keep_and_sell
==pressure_loxlen_fail==
//Lower Loxlen's faction's happiness 
The anthrocultivist certainly knows more than he's letting on, but he refuses to give in, and he's offended by your meddling. 

*[Buy the rights to the salvage.] ->buy_rights
*"[This is not the business of the Station Council.]->no_business
*[Confiscate the ship from the salvager.]->confiscate
==buy_rights==
{remove_resources(20)}
The salvager is ecstatic to get a materials payout for the old ship, and hands it over. For the next 30H, the station is chattering about the generosity and fairness of the council. 

Mateus: 'Waste of materials, if you ask me.' 
Barret: 'It'll keep them happy.' 
Loxlen: 'Well? When do I get to crack this egg?' 

*[Allow Loxlen to euthanize and reprint the ancient sleeper.] ->reprint
*[Keep the man frozen until you have the ability to resuscitate him]. ->long_freeze
*[Leave the ship and it's passenger in storage to sell off yourself]. ->keep_and_sell
*[Pressure Loxlen for more details on the man's religion.]->pressure_loxlen_test
==confiscate== 
{lost_moral_miner(10)} // this is how you call a prompt, it works for factions and resources
//Decrease miner's happiness 
Mateus and his security crew are more than happy to flex their muscles and take the ship off the salvager's hands. 

Barret: 'I'm the one they're going to complain to about this, you know.' 
Mateus: 'Make sure that's all they do, or I'll get involved again.' 
Loxlen: 'If you two could stop posturing at each other for one second, I'll need Barrett's help to crack this egg.' 

*[Allow Loxlen to euthanize and reprint the ancient sleeper.] ->reprint
*[Keep the man frozen until you have the ability to resuscitate him]. ->long_freeze
*[Leave the ship and it's passenger in storage to sell off yourself]. ->keep_and_sell
*[Pressure Loxlen for more details on the man's religion.]->pressure_loxlen_test
==reprint==
//Increase Loxlen's happiness
//Increase Loxlen's Anthrocultivation skill by 1
//Decrease miner's happiness
//Place a follow on event into the queue
The ancient sleeper has been reprinted. For now he is being sheltered by Loxlen through the adjustment period. The more superstitious of your crew consider him bad luck, and mutter under their breath for a whole 300H about the council.
->END
==long_freeze==
//Queue a followup event VERY far in the future in the event queue - Like 50 turns away
You store the man away until your medical facilities are capable of reviving him. Loxlen is unhappy, but respects the decision. It may be a long time until your medical facilities are up to scratch. 
->END
==keep_and_sell==
//Everyone hated that! 
//Queue in an event to follow up on it later. 
You store away the ship and it's passenger for the future. Perhaps you can turn a profit on it.
->END
===function CouncilSkillBiology(value)===
    ~ councilSkillBiology = value
===function CouncilSkillEngineering(value)===
    ~ councilSkillEngineering = value
===function CouncilSkillDiplomacy(value)===
    ~ councilSkillDiplomacy = value
===function CouncilSkillTrading(value)===
    ~ councilSkillTrading = value 
===function CouncilSkillSecurity(value)===
    ~ councilSkillSecurity = value
===function CouncilSkillProgramming(value)===
    ~ councilSkillProgramming = value
===function CouncilSkillAnthroministry(value)===
    ~ councilSkillAnthroministry = value
===function CouncilSkillXenology(value)===
    ~ councilSkillXenology = value

//This is where in Ink functions go.

















